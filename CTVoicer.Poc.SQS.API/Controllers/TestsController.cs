﻿using System.Threading.Tasks;
using CTVoicer.Poc.SQS.QueueService;
using Microsoft.AspNetCore.Mvc;

namespace CTVoicer.Poc.SQS.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestsController : ControllerBase
    {
        private readonly IQueueService _queueService;

        public TestsController(IQueueService queueService)
        {
            _queueService = queueService;
        }
        
        [HttpPost("PublishToAllQueues")]
        public async Task<ActionResult<bool>> PublishToAllQueues(string message)
        {
            // Standard Queue
            await _queueService.PublishToQueueAsync("CTVoicer-QueueTest-Standard-01", message);
            
            // Standard FIFO
            await _queueService.PublishToQueueAsync("CTVoicer-QueueTest-FIFO-01.fifo", message);
            
            return Ok();
        }
    }
}