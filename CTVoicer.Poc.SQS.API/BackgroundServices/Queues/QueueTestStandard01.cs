﻿using System;
using System.Threading.Tasks;
using CTVoicer.Poc.SQS.QueueService;
using Microsoft.Extensions.Logging;

namespace CTVoicer.Poc.SQS.API.BackgroundServices.Queues
{
    public class QueueTestStandard01 : QueueWorkerService
    {
        public QueueTestStandard01(IServiceProvider serviceProvider, ILogger<QueueTestStandard01> logger) : base(serviceProvider, logger)
        {
            QueueName = "CTVoicer-QueueTest-Standard-01";
        }
        
        protected override async Task<bool> ProcessMessageAsync(QueueMessage msg)
        {
            LogInformation(msg.Body);

            return await Task.FromResult(true);
        }
    }
}