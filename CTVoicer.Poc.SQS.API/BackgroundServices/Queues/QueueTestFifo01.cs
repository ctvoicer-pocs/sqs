using System;
using System.Threading.Tasks;
using CTVoicer.Poc.SQS.QueueService;
using Microsoft.Extensions.Logging;

namespace CTVoicer.Poc.SQS.API.BackgroundServices.Queues
{
    public class QueueTestFifo01 : QueueWorkerService
    {
        public QueueTestFifo01(IServiceProvider serviceProvider, ILogger<QueueTestFifo01> logger) : base(serviceProvider, logger)
        {
             QueueName = "CTVoicer-QueueTest-FIFO-01.fifo";
        }
        
        protected override async Task<bool> ProcessMessageAsync(QueueMessage msg)
        {
            LogInformation(msg.Body);

            return await Task.FromResult(true);
        }
    }
}