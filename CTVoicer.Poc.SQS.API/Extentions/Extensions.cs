using Amazon.SQS;
using CTVoicer.Poc.SQS.QueueService;
using Microsoft.Extensions.DependencyInjection;

namespace CTVoicer.Poc.SQS.API.Extentions
{
    public static class Extensions
    {
        public static IServiceCollection AddSqsService(this IServiceCollection services)
        {
            services.AddAWSService<IAmazonSQS>();
            services.AddScoped<IQueueService, SqsService>();
            
            return services;
        }
    }
}