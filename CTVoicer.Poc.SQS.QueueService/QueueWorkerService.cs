using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CTVoicer.Poc.SQS.QueueService
{
    public abstract class QueueWorkerService : BackgroundService
    {
        protected string QueueName { get; set; }
        private int MaxMessages { get; set; } = 10;

        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<QueueWorkerService> _logger;

        protected QueueWorkerService(IServiceProvider serviceProvider, ILogger<QueueWorkerService> logger)
        {
            _serviceProvider = serviceProvider;
            _logger = logger;
        }
        
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            using var scope = _serviceProvider.CreateScope();

            var queueService = scope.ServiceProvider.GetRequiredService<IQueueService>();
            
            var queueUrl = await queueService.GetQueueUrlAsync(QueueName);
            
            LogInformation($"Starting polling queue : {QueueName}");
            
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var messages = await queueService.ReceiveMessageAsync(queueUrl, MaxMessages);
                    if (!messages.Any())
                    {
                        continue;
                    }

                    foreach (var msg in messages)
                    {
                        var result = await ProcessMessageAsync(msg);
                        if (!result) continue;
                        
                        await queueService.DeleteMessageAsync( queueUrl, msg.Handle);
                    }

                }
                catch (Exception ex)
                {
                    LogError(ex.Message);
                }
                
            }
        }

        protected abstract Task<bool> ProcessMessageAsync(QueueMessage msg);

        protected virtual void LogInformation(string message)
        {
            _logger.LogInformation(message);
        }
        
        protected virtual void LogError(string message)
        {
            _logger.LogError(message);
        }
    }
}