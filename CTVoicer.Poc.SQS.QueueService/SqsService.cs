using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.SQS;
using Amazon.SQS.Model;
using Microsoft.Extensions.Logging;

namespace CTVoicer.Poc.SQS.QueueService
{
    public class SqsService : IQueueService
    {
        private readonly IAmazonSQS _amazonSqs;
        private ILogger<SqsService> _logger;

        public SqsService(IAmazonSQS amazonSqs, ILogger<SqsService> logger)
        {
            _amazonSqs = amazonSqs;
            _logger = logger;
        }

        public async Task<string> GetQueueUrlAsync(string queueName)
        {
            try
            {
                var response = await _amazonSqs.GetQueueUrlAsync(new GetQueueUrlRequest
                {
                    QueueName = queueName
                });

                return response.QueueUrl;
            }
            catch (QueueDoesNotExistException)
            {
                var request = new CreateQueueRequest
                {
                    QueueName = queueName,
                };

                if (queueName.Contains(".fifo"))
                {
                    request.Attributes = new Dictionary<string, string>
                    {
                        { "FifoQueue", "true" }
                    };
                }

                try
                {
                    var response = await _amazonSqs.CreateQueueAsync(request);
                    return response.QueueUrl;
                }
                catch (Exception e)
                {
                    _logger.LogInformation(e.Message);
                }

                return null;
            }
        }
        
        public async Task<bool> PublishToQueueAsync(string queueName, string message)
        {
            var queueUrl = await GetQueueUrlAsync(queueName);
            
            var request = new SendMessageRequest
            {
                MessageBody = message,
                QueueUrl = queueUrl
            };

            if (queueName.Contains(".fifo"))
            {
                request.MessageGroupId = "01";
                request.MessageDeduplicationId = Guid.NewGuid().ToString();
            }
            
            await _amazonSqs.SendMessageAsync(request);

            return true;
        }

        public async Task<List<QueueMessage>> ReceiveMessageAsync(string queueUrl, int maxMessages = 1)
        {
            var request = new ReceiveMessageRequest
            {
                QueueUrl = queueUrl,
                MaxNumberOfMessages = maxMessages
            };

            var messages = await _amazonSqs.ReceiveMessageAsync(request);
            
            return messages.Messages.Select(m => new QueueMessage
            {
                MessageId = m.MessageId,
                Body = m.Body,
                Handle = m.ReceiptHandle
            }).ToList();
        }

        public async Task DeleteMessageAsync(string queueUrl, string id)
        {
            await _amazonSqs.DeleteMessageAsync(new DeleteMessageRequest
            {
                QueueUrl = queueUrl,
                ReceiptHandle = id
            });
        }
    }
}