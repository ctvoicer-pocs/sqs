using System.Collections.Generic;
using System.Threading.Tasks;

namespace CTVoicer.Poc.SQS.QueueService
{
    public interface IQueueService
    {
        Task<string> GetQueueUrlAsync(string queueName);
        
        Task<bool> PublishToQueueAsync(string queueName, string message);
        
        Task<List<QueueMessage>> ReceiveMessageAsync(string queueUrl, int maxMessages = 1);

        Task DeleteMessageAsync(string queueUrl, string id);
    }
}